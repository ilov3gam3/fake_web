<?php

use App\Http\Controllers\ChuyenMucController;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::get('/get-active-categories', [ChuyenMucController::class, 'get_active_categories'])->name('get_active_categories');
Route::group(['middleware' => 'user'], function() {
});

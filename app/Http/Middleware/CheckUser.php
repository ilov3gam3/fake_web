<?php

namespace App\Http\Middleware;

use Closure;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class CheckUser
{
    public function handle(Request $request, Closure $next): Response
    {
        $user = Controller::getUserWithCookie(Cookie::get('token'));
        if ($user == null){
            return redirect('/auth');
        } else {
            return $next($request);
        }
//        $check  = Auth::guard('user')->check();
//        if($check) {
//            return $next($request);
//        } else {
//            return redirect('/auth');
//        }
    }
}

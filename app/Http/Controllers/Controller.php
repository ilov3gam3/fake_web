<?php

namespace App\Http\Controllers;

use App\Models\Customer;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Session;

class Controller extends BaseController
{
    use AuthorizesRequests, ValidatesRequests;
    public function test_cookie(){
        echo Cookie::get('token');
    }
    public static function getUserWithCookie($cookie){
        if ($cookie == null){
            return null;
        } else {
            $user = DB::table('customers')->where('token', $cookie)->first();
            if ($user == null){
                $resp = Http::get(env('USER_API_SERVER') . '/user/' . $cookie);
                if ($resp['user'] === null || !isset($resp['user'])){
                    return null;
                } else {
                    $check = DB::table('customers')->where('id', $resp['user'])->count() >= 1;
                    if (!$check){
                        DB::table('customers')->insert(array($resp['user']));
                    }
                    return $resp['user'];
                }
            } else {
                return $user;
            }
        }
    }
}

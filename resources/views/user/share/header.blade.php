<!-- Start header area -->
<header class="header__section">
    <div id="app_header">
        <div class="main__header header__sticky">
            <div class="container">
                <div class="main__header--inner position__relative d-flex justify-content-between align-items-center">
                    <div class="offcanvas__header--menu__open ">
                        <a class="offcanvas__header--menu__open--btn" href="javascript:void(0)" data-offcanvas>
                            <svg xmlns="http://www.w3.org/2000/svg" class="ionicon offcanvas__header--menu__open--svg"
                                viewBox="0 0 512 512">
                                <path fill="currentColor" stroke="currentColor" stroke-linecap="round"
                                    stroke-miterlimit="10" stroke-width="32" d="M80 160h352M80 256h352M80 352h352" />
                            </svg>
                            <span class="visually-hidden">Offcanvas Menu Open</span>
                        </a>
                    </div>
                    <div class="main__logo">
                        <h1 class="main__logo--title"><a class="main__logo--link" href="/"><img
                                    class="main__logo--img" src="/assets_user/img/logo/nav-log.webp" alt="logo-img"></a>
                        </h1>
                    </div>
                    <div class="header__search--widget d-none d-lg-block header__sticky--none">
                        <form class="d-flex header__search--form border-radius-5 col-12" action="#">
                            <div class="header__search--box">
                                <label>
                                    <input class="header__search--input" placeholder="Search For Products..."
                                        type="text">
                                </label>
                                <button class="header__search--button bg__primary text-white" aria-label="search button"
                                    type="submit">
                                    <svg width="16" height="16" viewBox="0 0 16 16" fill="none"
                                        xmlns="http://www.w3.org/2000/svg">
                                        <path
                                            d="M15.6952 14.4991L11.7663 10.5588C12.7765 9.4008 13.33 7.94381 13.33 6.42703C13.33 2.88322 10.34 0 6.66499 0C2.98997 0 0 2.88322 0 6.42703C0 9.97085 2.98997 12.8541 6.66499 12.8541C8.04464 12.8541 9.35938 12.4528 10.4834 11.6911L14.4422 15.6613C14.6076 15.827 14.8302 15.9184 15.0687 15.9184C15.2944 15.9184 15.5086 15.8354 15.6711 15.6845C16.0166 15.364 16.0276 14.8325 15.6952 14.4991ZM6.66499 1.67662C9.38141 1.67662 11.5913 3.8076 11.5913 6.42703C11.5913 9.04647 9.38141 11.1775 6.66499 11.1775C3.94857 11.1775 1.73869 9.04647 1.73869 6.42703C1.73869 3.8076 3.94857 1.67662 6.66499 1.67662Z"
                                            fill="currentColor" />
                                    </svg>
                                </button>
                            </div>
                        </form>
                    </div>
                    <div class="header__menu d-none d-lg-block header__sticky--block">
                        <nav class="header__menu--navigation">
                            <ul class="header__menu--wrapper d-flex">
                                <li class="header__menu--items">
                                    <a class="header__menu--link {{ Route::is('HomePageUser') ? 'active' : '' }}" href="/">Home Page </a>
                                </li>
                                {{-- <li class="header__menu--items">
                                    <a class="header__menu--link text-white active" href="blog.html">Blog
                                        <svg class="menu__arrowdown--icon" xmlns="http://www.w3.org/2000/svg"
                                            width="12" height="7.41" viewBox="0 0 12 7.41">
                                            <path d="M16.59,8.59,12,13.17,7.41,8.59,6,10l6,6,6-6Z"
                                                transform="translate(-6 -8.59)" fill="currentColor" opacity="0.7" />
                                        </svg>
                                    </a>
                                    <ul class="header__sub--menu">
                                        <li class="header__sub--menu__items"><a href="blog.html"
                                                class="header__sub--menu__link">Blog Grid</a></li>
                                        <li class="header__sub--menu__items"><a href="blog-details.html"
                                                class="header__sub--menu__link">Blog Details</a></li>
                                        <li class="header__sub--menu__items"><a href="blog-left-sidebar.html"
                                                class="header__sub--menu__link">Blog Left Sidebar</a></li>
                                        <li class="header__sub--menu__items"><a href="blog-right-sidebar.html"
                                                class="header__sub--menu__link">Blog Right Sidebar</a></li>
                                    </ul>
                                </li> --}}
                                <li class="header__menu--items">
                                    <a class="header__menu--link {{ Route::is('allProduct') ? 'active' : '' }}" href="{{ Route('allProduct') }}">Tất Cả Sản Phẩm </a>
                                </li>
                                <li class="header__menu--items">
                                    <a class="header__menu--link {{ Route::is('showNews') ? 'active' : '' }}" href="{{ Route('showNews') }}">Tin Tức </a>
                                </li>
                            </ul>
                        </nav>
                    </div>
                    @if (\App\Http\Controllers\Controller::getUserWithCookie(\Illuminate\Support\Facades\Cookie::get('token')) != null)
                        <div class="header__account header__sticky--none">
                            <ul class="header__account--wrapper d-flex align-items-center">
                                <li class="header__account--items d-none d-lg-block">
                                    <a class="header__account--btn" href="{{ Route('viewCustomerProfile') }}">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                            viewBox="0 0 24 24" fill="none" stroke="currentColor"
                                            stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                                            class=" -user">
                                            <path d="M20 21v-2a4 4 0 0 0-4-4H8a4 4 0 0 0-4 4v2"></path>
                                            <circle cx="12" cy="7" r="4"></circle>
                                        </svg>
                                        <span class="visually-hidden">My account</span>
                                    </a>
                                </li>

                                <li
                                    class="header__account--items  header__account--search__items mobile__d--block d-sm-2-none">
                                    <a class="header__account--btn search__open--btn" href="javascript:void(0)"
                                        data-offcanvas>
                                        <svg class="product__items--action__btn--svg"
                                            xmlns="http://www.w3.org/2000/svg" width="22.51" height="20.443"
                                            viewBox="0 0 512 512">
                                            <path
                                                d="M221.09 64a157.09 157.09 0 10157.09 157.09A157.1 157.1 0 00221.09 64z"
                                                fill="none" stroke="currentColor" stroke-miterlimit="10"
                                                stroke-width="32" />
                                            <path fill="none" stroke="currentColor" stroke-linecap="round"
                                                stroke-miterlimit="10" stroke-width="32"
                                                d="M338.29 338.29L448 448" />
                                        </svg>
                                        <span class="visually-hidden">Search</span>
                                    </a>
                                </li>

                                <li class="header__account--items d-none d-lg-block">
                                    <a class="header__account--btn" href="{{ Route('viewCustomerWishlist') }}">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                            viewBox="0 0 24 24" fill="none" stroke="currentColor"
                                            stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                                            class=" -heart">
                                            <path
                                                d="M20.84 4.61a5.5 5.5 0 0 0-7.78 0L12 5.67l-1.06-1.06a5.5 5.5 0 0 0-7.78 7.78l1.06 1.06L12 21.23l7.78-7.78 1.06-1.06a5.5 5.5 0 0 0 0-7.78z">
                                            </path>
                                        </svg>
                                        <span class="items__count" id="yeu_thich_1"></span>
                                    </a>
                                </li>
                                <li class="header__account--items header__minicart--items">
                                    <a v-on:click="loadData()" class="header__account--btn minicart__open--btn"
                                        href="javascript:void(0)" data-offcanvas>
                                        <svg xmlns="http://www.w3.org/2000/svg" width="22.706" height="22.534"
                                            viewBox="0 0 14.706 13.534">
                                            <g transform="translate(0 0)">
                                                <g>
                                                    <path data-name="Path 16787"
                                                        d="M4.738,472.271h7.814a.434.434,0,0,0,.414-.328l1.723-6.316a.466.466,0,0,0-.071-.4.424.424,0,0,0-.344-.179H3.745L3.437,463.6a.435.435,0,0,0-.421-.353H.431a.451.451,0,0,0,0,.9h2.24c.054.257,1.474,6.946,1.555,7.33a1.36,1.36,0,0,0-.779,1.242,1.326,1.326,0,0,0,1.293,1.354h7.812a.452.452,0,0,0,0-.9H4.74a.451.451,0,0,1,0-.9Zm8.966-6.317-1.477,5.414H5.085l-1.149-5.414Z"
                                                        transform="translate(0 -463.248)" fill="currentColor" />
                                                    <path data-name="Path 16788"
                                                        d="M5.5,478.8a1.294,1.294,0,1,0,1.293-1.353A1.325,1.325,0,0,0,5.5,478.8Zm1.293-.451a.452.452,0,1,1-.431.451A.442.442,0,0,1,6.793,478.352Z"
                                                        transform="translate(-1.191 -466.622)" fill="currentColor" />
                                                    <path data-name="Path 16789"
                                                        d="M13.273,478.8a1.294,1.294,0,1,0,1.293-1.353A1.325,1.325,0,0,0,13.273,478.8Zm1.293-.451a.452.452,0,1,1-.431.451A.442.442,0,0,1,14.566,478.352Z"
                                                        transform="translate(-2.875 -466.622)" fill="currentColor" />
                                                </g>
                                            </g>
                                        </svg>
                                        <span class="items__count" id="so_luong_cart_1"></span>
                                        <span class="minicart__btn--text">My Cart <br>
                                        </span>
                                    </a>
                                </li>
                                <li class="header__account--items">
                                    <a class="header__account--btn" href="{{ Route('customerLogOut') }}">
                                        <i class="fa-solid fa-arrow-right-from-bracket fa-lg"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <div class="header__account header__sticky--block">
                            <ul class="header__account--wrapper d-flex align-items-center">
                                <li class="header__account--items  header__account--search__items d-sm-2-none">
                                    <a class="header__account--btn search__open--btn" href="javascript:void(0)"
                                        data-offcanvas>
                                        <svg class="product__items--action__btn--svg"
                                            xmlns="http://www.w3.org/2000/svg" width="22.51" height="20.443"
                                            viewBox="0 0 512 512">
                                            <path
                                                d="M221.09 64a157.09 157.09 0 10157.09 157.09A157.1 157.1 0 00221.09 64z"
                                                fill="none" stroke="currentColor" stroke-miterlimit="10"
                                                stroke-width="32" />
                                            <path fill="none" stroke="currentColor" stroke-linecap="round"
                                                stroke-miterlimit="10" stroke-width="32"
                                                d="M338.29 338.29L448 448" />
                                        </svg>
                                        <span class="visually-hidden">Search</span>
                                    </a>
                                </li>
                                <li class="header__account--items d-none d-lg-block">
                                    <a class="header__account--btn" href="{{ Route('viewCustomerProfile') }}">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                            viewBox="0 0 24 24" fill="none" stroke="currentColor"
                                            stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                                            class=" -user">
                                            <path d="M20 21v-2a4 4 0 0 0-4-4H8a4 4 0 0 0-4 4v2"></path>
                                            <circle cx="12" cy="7" r="4"></circle>
                                        </svg>
                                        <span class="visually-hidden">My account</span>
                                    </a>
                                </li>
                                <li class="header__account--items d-none d-lg-block">
                                    <a class="header__account--btn" href="{{ Route('viewCustomerWishlist') }}">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                            viewBox="0 0 24 24" fill="none" stroke="currentColor"
                                            stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                                            class=" -heart">
                                            <path
                                                d="M20.84 4.61a5.5 5.5 0 0 0-7.78 0L12 5.67l-1.06-1.06a5.5 5.5 0 0 0-7.78 7.78l1.06 1.06L12 21.23l7.78-7.78 1.06-1.06a5.5 5.5 0 0 0 0-7.78z">
                                            </path>
                                        </svg>
                                        <span class="items__count" id="yeu_thich_2"></span>
                                    </a>
                                </li>
                                <li class="header__account--items header__minicart--items">
                                    <a v-on:click="loadData()" class="header__account--btn minicart__open--btn"
                                        href="javascript:void(0)" data-offcanvas>
                                        <svg xmlns="http://www.w3.org/2000/svg" width="22.706" height="22.534"
                                            viewBox="0 0 14.706 13.534">
                                            <g transform="translate(0 0)">
                                                <g>
                                                    <path data-name="Path 16787"
                                                        d="M4.738,472.271h7.814a.434.434,0,0,0,.414-.328l1.723-6.316a.466.466,0,0,0-.071-.4.424.424,0,0,0-.344-.179H3.745L3.437,463.6a.435.435,0,0,0-.421-.353H.431a.451.451,0,0,0,0,.9h2.24c.054.257,1.474,6.946,1.555,7.33a1.36,1.36,0,0,0-.779,1.242,1.326,1.326,0,0,0,1.293,1.354h7.812a.452.452,0,0,0,0-.9H4.74a.451.451,0,0,1,0-.9Zm8.966-6.317-1.477,5.414H5.085l-1.149-5.414Z"
                                                        transform="translate(0 -463.248)" fill="currentColor" />
                                                    <path data-name="Path 16788"
                                                        d="M5.5,478.8a1.294,1.294,0,1,0,1.293-1.353A1.325,1.325,0,0,0,5.5,478.8Zm1.293-.451a.452.452,0,1,1-.431.451A.442.442,0,0,1,6.793,478.352Z"
                                                        transform="translate(-1.191 -466.622)" fill="currentColor" />
                                                    <path data-name="Path 16789"
                                                        d="M13.273,478.8a1.294,1.294,0,1,0,1.293-1.353A1.325,1.325,0,0,0,13.273,478.8Zm1.293-.451a.452.452,0,1,1-.431.451A.442.442,0,0,1,14.566,478.352Z"
                                                        transform="translate(-2.875 -466.622)" fill="currentColor" />
                                                </g>
                                            </g>
                                        </svg>
                                        <span class="items__count" id="so_luong_cart_2"></span>
                                    </a>
                                </li>
                                <li class="header__account--items header__minicart--items">
                                    <a class="header__account--btn" href="{{ Route('customerLogOut') }}">
                                        <i class="fa-solid fa-arrow-right-from-bracket fa-lg"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    @else
                        <div class="header__account header__sticky--none">
                            <ul class="header__account--wrapper d-flex align-items-center">
                                <li class="header__account--items d-none d-lg-block">
                                    <a class="header__account--btn" href="/auth">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                            viewBox="0 0 24 24" fill="none" stroke="currentColor"
                                            stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                                            class=" -user">
                                            <path d="M20 21v-2a4 4 0 0 0-4-4H8a4 4 0 0 0-4 4v2"></path>
                                            <circle cx="12" cy="7" r="4"></circle>
                                        </svg>
                                        <span class="visually-hidden">My account</span>
                                    </a>
                                </li>

                                <li
                                    class="header__account--items  header__account--search__items mobile__d--block d-sm-2-none">
                                    <a class="header__account--btn search__open--btn" href="javascript:void(0)"
                                        data-offcanvas>
                                        <svg class="product__items--action__btn--svg"
                                            xmlns="http://www.w3.org/2000/svg" width="22.51" height="20.443"
                                            viewBox="0 0 512 512">
                                            <path
                                                d="M221.09 64a157.09 157.09 0 10157.09 157.09A157.1 157.1 0 00221.09 64z"
                                                fill="none" stroke="currentColor" stroke-miterlimit="10"
                                                stroke-width="32" />
                                            <path fill="none" stroke="currentColor" stroke-linecap="round"
                                                stroke-miterlimit="10" stroke-width="32"
                                                d="M338.29 338.29L448 448" />
                                        </svg>
                                        <span class="visually-hidden">Search</span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <div class="header__account header__sticky--block">
                            <ul class="header__account--wrapper d-flex align-items-center">
                                <li class="header__account--items  header__account--search__items d-sm-2-none">
                                    <a class="header__account--btn search__open--btn" href="javascript:void(0)"
                                        data-offcanvas>
                                        <svg class="product__items--action__btn--svg"
                                            xmlns="http://www.w3.org/2000/svg" width="22.51" height="20.443"
                                            viewBox="0 0 512 512">
                                            <path
                                                d="M221.09 64a157.09 157.09 0 10157.09 157.09A157.1 157.1 0 00221.09 64z"
                                                fill="none" stroke="currentColor" stroke-miterlimit="10"
                                                stroke-width="32" />
                                            <path fill="none" stroke="currentColor" stroke-linecap="round"
                                                stroke-miterlimit="10" stroke-width="32"
                                                d="M338.29 338.29L448 448" />
                                        </svg>
                                        <span class="visually-hidden">Search</span>
                                    </a>
                                </li>
                                <li class="header__account--items d-none d-lg-block">
                                    <a class="header__account--btn" href="/auth">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                            viewBox="0 0 24 24" fill="none" stroke="currentColor"
                                            stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                                            class=" -user">
                                            <path d="M20 21v-2a4 4 0 0 0-4-4H8a4 4 0 0 0-4 4v2"></path>
                                            <circle cx="12" cy="7" r="4"></circle>
                                        </svg>
                                        <span class="visually-hidden">My account</span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    @endif
                </div>
            </div>
        </div>
        <div class="header__bottom bg__primary">
            <div class="container">
                <div class="header__bottom--inner position__relative d-flex align-items-center">
                    <div class="categories__menu ">
                        <div class="categories__menu--header bg__secondary text-white d-flex align-items-center"
                            data-bs-toggle="collapse" data-bs-target="#categoriesAccordion">
                            <svg class="categories__list--icon" width="18" height="15" viewBox="0 0 18 15"
                                fill="none" xmlns="http://www.w3.org/2000/svg">
                                <rect width="13" height="1.5" fill="currentColor" />
                                <rect y="4.44434" width="18" height="1.5" fill="currentColor" />
                                <rect y="8.88892" width="15" height="1.5" fill="currentColor" />
                                <rect y="13.3333" width="17" height="1.5" fill="currentColor" />
                            </svg>

                            <span class="categories__menu--title">Select catagories</span>
                            <svg class="categories__arrowdown--icon" xmlns="http://www.w3.org/2000/svg"
                                width="12.355" height="8.394" viewBox="0 0 10.355 6.394">
                                <path d="M15.138,8.59l-3.961,3.952L7.217,8.59,6,9.807l5.178,5.178,5.178-5.178Z"
                                    transform="translate(-6 -8.59)" fill="currentColor" />
                            </svg>
                        </div>
                        <div class="dropdown__categories--menu border-radius-5 active collapse {{ Route::is('HomePageUser') ? 'show' : '' }}"
                            id="categoriesAccordion">
                            <ul class="d-none d-lg-block">
                                <template v-for="(key, value) in categories">
                                    <li class="categories__menu--items">
                                        <a class="categories__menu--link" :href=" '/product/' + value.id">
                                             @{{ value.ten_chuyen_muc }}
                                            <svg v-if="value.danh_muc_con.length > 0" class="categories__menu--right__arrow--icon"
                                                 xmlns="http://www.w3.org/2000/svg" width="17.007" height="16.831"
                                                 viewBox="0 0 512 512">
                                                <path fill="none" stroke="currentColor" stroke-linecap="round"
                                                      stroke-linejoin="round" stroke-width="48"
                                                      d="M184 112l144 144-144 144" />
                                            </svg>
                                        </a>
                                        <ul
                                            class="categories__submenu style2 border-radius-10 d-flex justify-content-between" style="width: 30rem">
                                            <li class="categories__submenu--items" style="width: 100%;"><a
                                                    class="categories__submenu--items__text"
                                                    :href=" '/product/' + value.id"><strong>@{{ value.ten_chuyen_muc }}</strong></a>
                                                <template v-for="(key2, value2) in value.danh_muc_con">
                                                    <ul class="categories__submenu--child" >
                                                        <li class="categories__submenu--child__items"><a
                                                                class="categories__submenu--child__items--link"
                                                                :href=" '/product/' + value2.id">@{{ value2.ten_chuyen_muc }}</a></li>
                                                    </ul>
                                                </template>
                                            </li>
                                        </ul>
                                    </li>
                                </template>
                            </ul>
                            <nav class="category__mobile--menu">
                                <ul class="d-none d-lg-block">
                                    <template v-for="(key, value) in categories">
                                        <li class="categories__menu--items">
                                            <a class="categories__menu--link" :href=" '/product/' + value.id">
                                                @{{ value.ten_chuyen_muc }}
                                                <svg v-if="value.danh_muc_con.length > 0" class="categories__menu--right__arrow--icon"
                                                     xmlns="http://www.w3.org/2000/svg" width="17.007" height="16.831"
                                                     viewBox="0 0 512 512">
                                                    <path fill="none" stroke="currentColor" stroke-linecap="round"
                                                          stroke-linejoin="round" stroke-width="48"
                                                          d="M184 112l144 144-144 144" />
                                                </svg>
                                            </a>
                                            <ul
                                                class="categories__submenu style2 border-radius-10 d-flex justify-content-between" style="width: 30rem">
                                                <li class="categories__submenu--items" style="width: 100%;"><a
                                                        class="categories__submenu--items__text"
                                                        :href=" '/product/' + value.id"><strong>@{{ value.ten_chuyen_muc }}</strong></a>
                                                    <template v-for="(key2, value2) in value.danh_muc_con">
                                                        <ul class="categories__submenu--child" >
                                                            <li class="categories__submenu--child__items"><a
                                                                    class="categories__submenu--child__items--link"
                                                                    :href=" '/product/' + value2.id">@{{ value2.ten_chuyen_muc }}</a></li>
                                                        </ul>
                                                    </template>
                                                </li>
                                            </ul>
                                        </li>
                                    </template>
                                </ul>
                            </nav>
                        </div>
                    </div>
                    <div class="header__right--area d-flex justify-content-between align-items-center">
                        <div class="header__menu">
                            <nav class="header__menu--navigation">
                                <ul class="header__menu--wrapper d-flex">
                                    <li class="header__menu--items">
                                        <a class="header__menu--link text-white {{ Route::is('HomePageUser') ? 'active' : '' }}" href="/">Home Page </a>
                                    </li>
                                    <li class="header__menu--items">
                                        <a class="header__menu--link text-white {{ Route::is('allProduct') ? 'active' : '' }}"
                                            href="{{ Route('allProduct') }}">Tất Cả Sản Phẩm </a>
                                    </li>
                                    <li class="header__menu--items">
                                        <a class="header__menu--link text-white {{ Route::is('showNews') ? 'active' : '' }}" href="{{ Route('showNews') }}">Tin Tức </a>
                                    </li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="predictive__search--box ">
            <div class="predictive__search--box__inner">
                <h2 class="predictive__search--title">Search Products</h2>
                <form class="predictive__search--form" action="#">
                    <label>
                        <input class="predictive__search--input" placeholder="Search Here" type="text">
                    </label>
                    <button class="predictive__search--button text-white" aria-label="search button"><svg
                            class="product__items--action__btn--svg" xmlns="http://www.w3.org/2000/svg"
                            width="30.51" height="25.443" viewBox="0 0 512 512">
                            <path d="M221.09 64a157.09 157.09 0 10157.09 157.09A157.1 157.1 0 00221.09 64z"
                                fill="none" stroke="currentColor" stroke-miterlimit="10" stroke-width="32" />
                            <path fill="none" stroke="currentColor" stroke-linecap="round"
                                stroke-miterlimit="10" stroke-width="32" d="M338.29 338.29L448 448" />
                        </svg> </button>
                </form>
            </div>
            <button class="predictive__search--close__btn" aria-label="search close" data-offcanvas>
                <svg class="predictive__search--close__icon" xmlns="http://www.w3.org/2000/svg" width="40.51"
                    height="30.443" viewBox="0 0 512 512">
                    <path fill="currentColor" stroke="currentColor" stroke-linecap="round"
                        stroke-linejoin="round" stroke-width="32" d="M368 368L144 144M368 144L144 368" />
                </svg>
            </button>
        </div>
        @php
            $checklogin = \App\Http\Controllers\Controller::getUserWithCookie(\Illuminate\Support\Facades\Cookie::get('token')) != null;
        @endphp
        @if ($checklogin)
            <div class="offCanvas__minicart">
                <div class="minicart__header ">
                    <div class="minicart__header--top d-flex justify-content-between align-items-center">
                        <h3 class="minicart__title"> Shopping Cart</h3>
                        <button class="minicart__close--btn" aria-label="minicart close btn" data-offcanvas>
                            <svg class="minicart__close--icon" xmlns="http://www.w3.org/2000/svg"
                                viewBox="0 0 512 512">
                                <path fill="currentColor" stroke="currentColor" stroke-linecap="round"
                                    stroke-linejoin="round" stroke-width="32"
                                    d="M368 368L144 144M368 144L144 368" />
                            </svg>
                        </button>
                    </div>
                    <p class="minicart__header--desc">The organic foods products are limited</p>
                </div>
                <div class="minicart__product">
                    <template v-for="(k, v) in list">
                        <div class="minicart__product--items d-flex">
                            <div class="minicart__thumb">
                                <a v-bind:href="'/product-detail/' + v.id_san_pham"><img v-bind:src="v.hinh_anh"
                                        alt="prduct-img"></a>
                            </div>
                            <div class="minicart__text">
                                <h4 class="minicart__subtitle"><a
                                        v-bind:href="'/product-detail/' + v.id_san_pham">@{{ v.ten_san_pham }}</a>
                                </h4>
                                <div class="minicart__price">
                                    <span class="minicart__current--price">@{{ numberFormat(v.gia_khuyen_mai) }}</span>
                                    <span class="minicart__old--price">@{{ numberFormat(v.gia_ban) }}</span>
                                </div>
                                <div class="minicart__text--footer d-flex align-items-center">
                                    <div class="quantity__box minicart__quantity">
                                        <button type="button" v-on:click="tru(v)"
                                            class="quantity__value decrease" aria-label="Giảm số lượng"
                                            value="Giảm giá trị">-</button>
                                        <label>
                                            <input v-model="v.so_luong" v-on:change="update(v)" type="number"
                                                class="quantity__number" data-counter />
                                        </label>
                                        <button type="button" v-on:click="cong(v)"
                                            class="quantity__value increase" aria-label="Tăng số lượng"
                                            value="Tăng giá trị">+</button>
                                    </div>
                                    <button v-on:click="destroy(v)" class="minicart__product--remove"
                                        type="button">Remove</button>
                                </div>
                            </div>
                        </div>
                    </template>
                </div>
                <div class="minicart__amount">
                    <div class="minicart__amount_list d-flex justify-content-between">
                        <span>Sub Total:</span>
                        <span id="subTotal"><b></b></span>
                    </div>

                </div>
                <div class="minicart__button d-flex justify-content-center">
                    <a class="primary__btn minicart__button--link" href="{{ Route('viewCheckout') }}">Checkout</a>
                </div>
            </div>
        @endif
    </div>
    <!-- End serch box area -->
    <script>
        $(document).ready(function() {
            new Vue({
                el: '#app_header',
                data: {
                    list: [],
                    categories: [],
                    test : 'test123'
                },
                created() {
                    this.loadData();
                    this.loadDataWishlist();
                    this.load_categories();
                },
                methods: {
                    loadData() {
                        axios
                            .get('{{ Route('dataGioHang') }}')
                            .then((res) => {
                                this.list = res.data.data;
                                subTotal = res.data.subTotal;
                                so_luong_cart_1 = res.data.so_luong;
                                so_luong_cart_2 = res.data.so_luong;
                                $("#so_luong_cart_1").html(so_luong_cart_1);
                                $("#so_luong_cart_2").html(so_luong_cart_2);
                                $("#subTotal").html(this.numberFormat(subTotal));
                                // document.getElementById("so_luong_cart").innerHTML = so_luong_cart;
                            })
                            .catch((res) => {
                                $.each(res.response.data.errors, function(k, v) {
                                    toastr.error(v[0], 'Error');
                                });
                            });
                    },
                    loadDataWishlist() {
                        axios
                            .post('{{ Route('dataCustomerWishlist') }}')
                            .then((res) => {
                                yeu_thich_1 = res.data.yeu_thich;
                                yeu_thich_2 = res.data.yeu_thich;
                                $("#yeu_thich_1").html(yeu_thich_1);
                                $("#yeu_thich_2").html(yeu_thich_2);
                            })
                            .catch((res) => {
                                $.each(res.response.data.errors, function(k, v) {
                                    toastr.error(v[0], 'Error');
                                });
                            });
                    },
                    numberFormat(number) {
                        return new Intl.NumberFormat('vi-VI', {
                            style: 'currency',
                            currency: 'VND'
                        }).format(number);
                    },
                    destroy(value) {
                        axios
                            .post('{{ Route('deleteGioHang') }}', value)
                            .then((res) => {
                                if (res.data.status) {
                                    toastr.success(res.data.message, "Success!");
                                    this.loadData();
                                } else {
                                    toastr.error(res.data.message, "Error!");
                                    this.loadData();
                                }
                            })
                            .catch((res) => {
                                $.each(res.response.data.errors, function(k, v) {
                                    toastr.error(v[0]);
                                });
                            });
                    },
                    cong(v) {
                        v.so_luong++;
                        this.update(v);
                    },
                    tru(v) {
                        v.so_luong--;
                        this.update(v);
                    },
                    update(v) {
                        axios
                            .post('{{ Route('updateGioHang') }}', v)
                            .then((res) => {
                                if (res.data.status) {
                                    // toastr.success(res.data.message, "Success!");
                                    this.loadData();
                                } else {
                                    toastr.error(res.data.message, "Error!");
                                    this.loadData();
                                }
                            })
                            .catch((res) => {
                                $.each(res.response.data.errors, function(k, v) {
                                    toastr.error(v[0]);
                                });
                            });
                    },
                    load_categories(){
                        axios.get('/api/get-active-categories')
                            .then((res) =>{
                                this.categories = res.data
                                for (let i = 0; i < this.categories.length; i++) {
                                    this.categories[i].danh_muc_con = []
                                    for (let j = 0; j < this.categories.length; j++) {
                                        if (this.categories[j].id_chuyen_muc_cha === this.categories[i].id){
                                            this.categories[i].danh_muc_con.push(this.categories[j])
                                        }
                                    }
                                }
                                let temp = []
                                for (let i = 0; i < this.categories.length; i++){
                                    if (this.categories[i].id_chuyen_muc_cha === 0){
                                        temp.push(this.categories[i])
                                    }
                                }
                                this.categories = temp;
                            })
                    }
                },
            });
        })
    </script>
</header>
<!-- End header area -->
